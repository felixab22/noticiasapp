import { Component, OnInit, Input } from '@angular/core';
import { NoticiasService } from 'src/app/services/noticias.service';
import { Article } from 'src/app/intefaces/noticias.intefaces';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss'],
})
export class NoticiasComponent implements OnInit {

 @Input() noticias: Article[] = [];
  constructor(
    private _NoticiasSrv: NoticiasService
  ) {}

  ngOnInit(){
  }

}