import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../../services/noticias.service';
import { Article } from '../../intefaces/noticias.intefaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  noticias: Article[] = [];
  constructor(
    private _NoticiasSrv: NoticiasService
  ) {}

  ngOnInit(){
    this.listatTopLine();
  }
  listatTopLine(event?) {
    console.log('entro');    
    this._NoticiasSrv.getTopHeadLine().subscribe(res=> {
      console.log(res);
      if(res.articles.length === 0){
        event.target.disabled = true;
        event.target.complete();
        return
      }
      this.noticias.push(...res.articles);      
      if(event){
        event.target.complete();
      }

    })
  }
  loadData(event){    
    this.listatTopLine(event);
  }
}
