import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment, IonContent } from '@ionic/angular';
import { NoticiasService } from 'src/app/services/noticias.service';
import { Article } from 'src/app/intefaces/noticias.intefaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  @ViewChild('segmento',{static: true}) segment: IonSegment;
  @ViewChild('content',{static: true}) content: IonContent;
  noticias: Article[] = [];
  categorias= ["business","entertainment","general","health","science","sports","technology"]
  
 constructor(
  private _NoticiasSrv: NoticiasService
 ) {}
 ngOnInit() {
  this.segment.value = this.categorias[0];
  this.listarXCategorisa(this.categorias[0]);
}
  segmentChanged(event) {
    this.noticias = [];
    // this.content.scrollToTop().then(() => {
    //   console.log('scroll to top');
    // });
    this.content.scrollToTop();
    this.listarXCategorisa(event.detail.value);
    
  }
  listarXCategorisa(categorias,event?) {
    this._NoticiasSrv.getLineCategoria(categorias).subscribe(res => {
      // console.log(res);
      if(res.articles.length === 0){
        event.target.disabled = true;
        event.target.complete();
        return
      }
      this.noticias.push(...res.articles)
      if(event){
        event.target.complete();
      }
    })
  }
  loadData(event) {
    // console.log(event);    
    this.listarXCategorisa(this.segment.value,event);
  }
}
