import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaTopHeadLines } from '../intefaces/noticias.intefaces';
import { environment } from '../../environments/environment.prod';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-key': apiKey
})


@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  headLinePage=0;
  categoriaActual='';
   categoriaPage=0;

  
  constructor(
    private _htpp: HttpClient
  ) { }

  getTopHeadLine(){    
    this.headLinePage++; 
    return this._htpp.get<RespuestaTopHeadLines>(`https://newsapi.org/v2/everything?q=bitcoin&from=2019-12-27&sortBy=publishedAt&apiKey=147a81cedc49451a850a2a2c46bf9fde&page=${this.headLinePage}`);
  }
  getLineCategoria(categoria){
    if(this.categoriaActual === categoria) {
      this.categoriaPage++;
    } else {
      this.categoriaPage=1;
      this.categoriaActual = categoria;
    }
    return this._htpp.get<RespuestaTopHeadLines>(`https://newsapi.org/v2/top-headlines?country=us&category=${categoria}&apiKey=147a81cedc49451a850a2a2c46bf9fde&page=${this.categoriaPage}`);
  }
}
